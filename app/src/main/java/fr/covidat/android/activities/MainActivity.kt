/*
 * Copyright (c) 2020 Codefish <codefish@online.fr>
 * Software released under the terms of the CeCILL 2.1 license [https://cecill.info/]
 */

package fr.covidat.android.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.navigation.NavigationView
import fr.covidat.android.BuildConfig
import fr.covidat.android.R
import fr.covidat.android.attachments.model.Attachments
import fr.covidat.android.attestation.model.Attestation
import fr.covidat.android.attestation.model.AttestationKind
import fr.covidat.android.attestation.ui.AttestationGeneratorFragmentDirections
import fr.covidat.android.service.GeneratorService
import fr.covidat.android.utils.SoundPlayer
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.IOException


/**
 * The main activity of the application hosting the fragments
 */
class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration

    private val baseTopLevelDestinations = setOf( R.id.nav_attestation, R.id.nav_qrcode, R.id.nav_towns, R.id.nav_profile, R.id.nav_attachments, R.id.nav_service, R.id.nav_about)
    private val generatorNavIds = AttestationKind.values().map { it.navId }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(generatorNavIds.toSet() + baseTopLevelDestinations, drawerLayout)
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

        navView.getHeaderView(0).findViewById<TextView>(R.id.drawerTitle).text =
            "${getString(R.string.app_name)} ${BuildConfig.VERSION_NAME}"

        GeneratorService.start(this)

        addGeneratorsToMenu()

        manageIntent(intent, savedInstanceState != null)
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        Log.d(javaClass.name, "Received intent: $intent")
        intent?.let { manageIntent(it) }
    }

    private fun manageIntent(intent: Intent, restart: Boolean = false) {
        Log.d(javaClass.name, "Manage intent: $intent")
        val navController = findNavController(R.id.nav_host_fragment)
        val action = AttestationGeneratorFragmentDirections.gotoAttestation()
        val goto = intent.getIntExtra("goto", -1)
        if (goto != -1 && !restart)
            navController.navigate(action)

        /*if (intent.action in arrayOf(Intent.ACTION_SEND, Intent.ACTION_SEND_MULTIPLE) ) {
            Log.d(javaClass.name, "Having received send intent: $intent")
            CoroutineScope(Dispatchers.Main).launch {
                withContext(Dispatchers.Default) {
                    Attachments.make(this@MainActivity).addAttachments(this@MainActivity, intent)
                }
                val action2 = AttestationGeneratorFragmentDirections.gotoAttachments()
                navController.navigate(action2)
            }
        }*/
    }

    /** Add the attestation generators to the navigation menu */
    private fun addGeneratorsToMenu() {
        val navigationView = findViewById<View>(R.id.nav_view) as NavigationView
        val menu = navigationView.menu
        val contemporaryAttestationKinds = AttestationKind.values().filter { it.isContemporary }
        contemporaryAttestationKinds.forEach { menu.add(Menu.NONE, it.navId, 0, "Créer ${getString(it.usualName)}") }
    }

    override fun onStart() {
        super.onStart()
        SoundPlayer.active = false
    }

    override fun onStop() {
        super.onStop()
        SoundPlayer.active = true
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.options, menu)
        return true
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        val attestation = Attestation.getLatestAttestation(this)
        arrayOf(R.id.viewAttestationMenuItem, R.id.shareAttestationMenuItem, R.id.saveAttestationMenuItem).forEach {
            menu?.findItem(it)?.isEnabled = attestation != null
        }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        val attestation = Attestation.getLatestAttestation(this)
        when(item.itemId) {
            R.id.viewAttestationMenuItem -> attestation?.launchPDFIntent(this, Intent.ACTION_VIEW)
            R.id.shareAttestationMenuItem -> attestation?.launchPDFIntent(this, Intent.ACTION_SEND)
            R.id.saveAttestationMenuItem -> saveAttestation()
            else -> return false
        }
        return true
    }

    fun saveAttestation() {
        Intent(Intent.ACTION_CREATE_DOCUMENT)
            .setType("application/pdf")
            .apply { startActivityForResult(this, CREATE_PDF_REQUEST_CODE) }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CREATE_PDF_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            val uri = data?.data
            uri?.let {
                val attestation = Attestation.getLatestAttestation(this)
                try {
                    attestation?.copyPDFAttestation(uri)
                    Toast.makeText(this, "Copie réussie", Toast.LENGTH_LONG).show()
                } catch (e: IOException) {
                    Log.e(javaClass.name, "Error while copying", e)
                    Toast.makeText(this, "Erreur lors de la copie: " + e, Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    companion object {
        const val CREATE_PDF_REQUEST_CODE = 1
    }
}
