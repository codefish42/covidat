/*
 * Copyright (c) 2020 Codefish <codefish@online.fr>
 * Software released under the terms of the CeCILL 2.1 license [https://cecill.info/]
 */

package fr.covidat.android.attachments.model

import android.content.Context
import android.content.Intent
import android.content.Intent.EXTRA_STREAM
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Parcelable
import android.util.Log
import androidx.core.net.toUri
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.File
import java.security.MessageDigest

class Attachments {
    companion object {
        const val ATTACHMENT_DIRECTORY = "attachments"

        private fun getAttachmentPath(context: Context): File {
            return context.getDir(ATTACHMENT_DIRECTORY, Context.MODE_PRIVATE)
        }

        fun make(context: Context): Attachments {
            val obj = Attachments()
            obj.loadAttachments(context)
            return obj
        }
    }

    private val attachments = arrayListOf<File>()

    fun loadAttachments(context: Context) {
        getAttachmentPath(context).listFiles().sortedBy { it.lastModified() }.forEach { attachments.add(it) }
    }

    val size: Int get() = attachments.size

    /** Get the bitmap related to an attachment */
    fun getAttachmentBitmap(i: Int) = attachments[i].inputStream().use { BitmapFactory.decodeStream(it) }

    fun getAttachmentFile(i: Int) = attachments[i]

    val allAttachments: Sequence<Bitmap> get() = (0 until size).asSequence().map { getAttachmentBitmap(it) }

    /** Add new attachments from an intent */
    fun addAttachments(context: Context, intent: Intent) =
        when (intent.action) {
            Intent.ACTION_SEND ->
                (intent.getParcelableExtra<Parcelable>(EXTRA_STREAM) as? Uri)?.let { addAttachment(context, it) }
            Intent.ACTION_SEND_MULTIPLE ->
                (intent.getParcelableArrayListExtra<Parcelable>(EXTRA_STREAM))?.let {
                    it.mapNotNull { x -> x as? Uri }.forEach { x -> addAttachment(context, x)  } }
            else -> {}
        }

    fun computeFingerprint(context: Context, uri: Uri): ByteArray? {
        val md = MessageDigest.getInstance("SHA-256")
        return context.contentResolver.openInputStream(uri)?.use {
            val buffer = ByteArray(2048)
            var r = it.read(buffer)
            while (r >= 0) {
                md.update(buffer, 0, r)
                r = it.read(buffer)
            }
            md.digest()
        }
    }

    fun ByteArray.toHexString() = joinToString("") { "%02x".format(it) }

    fun copyUri(context: Context, uri: Uri) {
        val fp = computeFingerprint(context, uri)?.toHexString()?.substring(0, 32)
        if (fp == null) Unit
        else {
            val path = getAttachmentPath(context).resolve(fp)
            if (path.exists()) Unit
            else {
                context.contentResolver.openInputStream(uri)?.use {input ->
                    path.outputStream()?.use { output -> input.copyTo(output) }
                }
            }
        }
    }

    /** Add the attachment specified by the URI */
    fun addAttachment(context: Context, uri: Uri) {
        val mimeType = context.contentResolver.getType(uri)
        Log.d(javaClass.name, "Adding attachment uri=$uri of type=$mimeType")
        when(mimeType) {
            "image/png", "image/jpeg" -> {
                // copy the file to our local directory
                copyUri(context, uri)
            }
        }
    }

}