/*
 * Copyright (c) 2020 Codefish <codefish@online.fr>
 * Software released under the terms of the CeCILL 2.1 license [https://cecill.info/]
 */

package fr.covidat.android.attachments.ui

import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.github.chrisbanes.photoview.PhotoView
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import fr.covidat.android.R
import fr.covidat.android.attachments.model.Attachments
import fr.covidat.android.frenchtowns.model.Coords
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

/**
 * A fragment displaying a list of French towns organized by departments.
 */
class AttachmentsFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        inflater.inflate(R.layout.fragment_attachments, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val viewPager = view.findViewById<ViewPager2>(R.id.pager)
        viewPager.adapter = AttachmentsAdapter(this, Attachments.make(requireContext()))
        val tabLayout = view.findViewById<TabLayout>(R.id.tab_layout)
        TabLayoutMediator(tabLayout, viewPager) { tab, position ->
            tab.text = "${(position + 1)}"
        }.attach()
    }
}

class AttachmentsAdapter(fragment: Fragment, val attachments: Attachments): FragmentStateAdapter(fragment) {
    override fun getItemCount() = attachments.size

    override fun createFragment(position: Int): Fragment = AttachmentFragment().apply { arguments = Bundle().apply {
        putString("path", attachments.getAttachmentFile(position).path)
    } }
}

class AttachmentFragment: Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val photoView = PhotoView(context)
        CoroutineScope(Dispatchers.Main).launch {
            val bm = withContext(Dispatchers.Default) { BitmapFactory.decodeFile(arguments?.getString("path")) }
            photoView.setImageBitmap(bm)
            photoView.setZoomable(true)
        }
        return photoView
    }
}
