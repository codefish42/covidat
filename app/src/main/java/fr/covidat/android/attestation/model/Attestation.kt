/*
 * Copyright (c) 2020 Codefish <codefish@online.fr>
 * Software released under the terms of the CeCILL 2.1 license [https://cecill.info/]
 */

package fr.covidat.android.attestation.model

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.graphics.pdf.PdfDocument
import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import android.graphics.*
import android.graphics.Bitmap
import android.net.Uri
import android.util.Log
import android.widget.Toast
import androidx.core.content.FileProvider
import androidx.core.graphics.applyCanvas
import com.google.gson.Gson
import fr.covidat.android.attachments.model.Attachments
import fr.covidat.android.templates.OriginalAttestationTemplate
import java.io.File
import java.io.IOException
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class Attestation(val context: Context, val attestationData: AttestationData, private val directory: File) {

    val template = attestationData.attestationKind.template

    companion object {
        private val paint = Paint()
        private val bounds = Rect()

        const val DEFAULT_FONT_SIZE = 30
        const val MIN_FONT_SIZE = 10

        const val ATTESTATION_DIRECTORY = "attestations"
        const val PDF_FILENAME = "attestation.pdf"
        const val ATTESTATION_PNG_FILENAME = "attestation.png"
        const val QRCODE_PNG_FILENAME = "qrcode.png"
        const val DATA_FILENAME = "data.json"

        private fun setIdealFontSize(text: String, width: Int, height: Int, minSize: Int, defaultSize: Int) {
            paint.isAntiAlias = true
            paint.textSize = defaultSize.toFloat()
            paint.getTextBounds(text, 0, text.length,
                bounds
            )
            while (bounds.width() > width && bounds.height() > height && paint.textSize > minSize) {
                paint.textSize--
                paint.getTextBounds(text, 0, text.length,
                    bounds
                )
            }
        }

        private fun Canvas.drawText(
            text: String,
            position: IntArray,
            defaultSize: Int = DEFAULT_FONT_SIZE,
            minSize: Int = MIN_FONT_SIZE
        ) {
            setIdealFontSize(
                text,
                position[2],
                position[3],
                minSize,
                defaultSize
            )
            this.drawText(text, (position[0] + OriginalAttestationTemplate.offset[0]).toFloat(), (position[1] + OriginalAttestationTemplate.offset[1]).toFloat(),
                paint
            )
        }

        private fun Canvas.drawBitmap(bitmap: Bitmap, position: IntArray) {
            drawBitmap(bitmap, (position[0] + OriginalAttestationTemplate.offset[0]).toFloat(), (position[1] + OriginalAttestationTemplate.offset[1]).toFloat(),
                paint
            )
        }

        fun getLatestAttestation(context: Context): Attestation? {
            val parentDir = context.applicationContext.filesDir.resolve(ATTESTATION_DIRECTORY)
            val dir = if (parentDir.exists()) parentDir.listFiles().maxByOrNull { it.name } else null
            if (dir == null) return null
            else {
                val json = dir.resolve(DATA_FILENAME).readText()
                val data = try { Gson().fromJson(json, AttestationData::class.java) } catch (e: Exception) { null }
                if (data != null && data.attestationKind != null) return Attestation(
                    context,
                    data,
                    dir
                ) else return null
            }
        }

        val Long.iso8601: String get() {
            return SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US).let {
                it.setTimeZone(TimeZone.getTimeZone("UTC"))
                it.format(Date(this))
            }
        }

        fun create(context: Context, data: AttestationData): Attestation {
            val parentDir = context.applicationContext.filesDir.resolve(ATTESTATION_DIRECTORY)
            // create directory for the attestation
            val dir = parentDir.resolve(data.circumstances.date.iso8601)
            if (dir.mkdirs()) {
                val json = Gson().toJson(data)
                dir.resolve(DATA_FILENAME).writeText(json)
                return Attestation(context, data, dir)
            } else
                throw IOException("Cannot create directory $dir")
        }

        /** Keep only the directory of the latest attestation */
        fun removeOldAttestations(context: Context) {
            val parentDir = context.applicationContext.filesDir.resolve(ATTESTATION_DIRECTORY)
            if (parentDir != null) {
                val l = parentDir.listFiles().sortedBy { it.name }
                if (l.size > 1) {
                    val l2 = l.subList(0, l.size - 1)
                    l2.forEach { it.deleteRecursively() }
                }
            }
        }
    }



    fun generateQRCode(size: Pair<Int, Int>): Bitmap {
        val matrix = MultiFormatWriter().encode(
            attestationData.attestationKind.template.getQRText(attestationData),
            BarcodeFormat.QR_CODE, size.first, size.second
        )
        val bm = Bitmap.createBitmap(size.first, size.second, Bitmap.Config.ARGB_8888)
        val pixels = IntArray(size.first)
        for (j in 0.until(size.second)) {
            // write row by row in the bitmap
            for (i in 0.until(size.first))
                pixels[i] = if (matrix.get(i, j)) Color.BLACK else Color.WHITE
            bm.setPixels(pixels, 0, size.first, 0, j, size.first, 1)
        }
        return bm
    }



    private fun drawForm(canvas: Canvas) {
        val options = BitmapFactory.Options()
        options.inScaled = false
        val bm = BitmapFactory.decodeResource(context.resources, template.resource, options)
        val d = attestationData
        canvas.apply {
            val rect = Rect(0,0, template.pageSize[0], template.pageSize[1])
            drawBitmap(bm, rect, rect, paint)
            val map = mapOf(
                template.firstName to d.personalData.firstName,
                template.lastName to d.personalData.lastName,
                template.completeName to "${d.personalData.firstName} ${d.personalData.lastName}",

                template.birthDate to d.personalData.birthDate,
                template.birthPlace to d.personalData.birthPlace,
                template.completeBirth to "${d.personalData.birthDate} à ${d.personalData.birthPlace}",

                template.street to d.personalData.street,
                template.town to "${d.personalData.town?.zip} ${d.personalData.town?.name}",
                template.completeAddress to "${d.personalData.street} ${d.personalData.town?.zip} ${d.personalData.town?.name}",

                template.destinationTown to "${d.circumstances.destination?.name} (${d.circumstances.destination?.zip})",
                template.destinationDepartment to "${d.circumstances.destination?.zip?.substring(0..1)}",

                template.date to d.circumstances.day,
                template.dayOfMonth to d.circumstances.day.substring(0..1),
                template.month to d.circumstances.day.substring(3..4),
                template.hour to d.circumstances.hour,
                template.minute to d.circumstances.minute,
                template.hourMinute to "${d.circumstances.hour}:${d.circumstances.minute}",

                template.place to d.personalData.town?.name,
                template.creationDateLabel to "Date de création:"
            )

            map.forEach { entry -> (0 until (entry.key.size/4)).forEach {
                entry.value?.apply {
                    drawText(this, entry.key.sliceArray(it * 4 until (it + 1) * 4))
                }
            } }

            d.circumstances.reasons.mapNotNull { template.reason[it] }.forEach {
                val a = it
                (0 until it.size / 4).forEach { i -> drawText("x", a.sliceArray(i * 4 until (i+1) * 4), 50) }
            }

            val qr = generateQRCode(template.qr[2] to template.qr[3])
            drawBitmap(qr, template.qr)
            if (template.creationDateLabel.isNotEmpty()) drawText("Date de création:", template.creationDateLabel, 20)
            if (template.creationDate.isNotEmpty())
                drawText(
                    "${d.circumstances.day} à ${d.circumstances.hourMinute}",
                    template.creationDate, 20
                )
        }
    }

    private fun drawLargeQRCode(canvas: Canvas) {
        val qr = generateQRCode(template.qr2[2] to template.qr2[3])
        canvas.drawBitmap(qr, template.qr2)
    }

    private fun generatePDFAttestation() {
        val doc = PdfDocument()
        arrayOf(this::drawForm, this::drawLargeQRCode).forEachIndexed { i, gen ->
            val pageInfo = PdfDocument.PageInfo.Builder(
                template.pageSize[0],
                template.pageSize[1],
                i + 1
            ).create()
            val page = doc.startPage(pageInfo)
            gen(page.canvas)
            doc.finishPage(page)
        }
        // add the attachments
        /*Attachments.make(context).allAttachments.forEachIndexed {  i, bm->
            Log.d(javaClass.name, "Adding attachment $i")
            val pageInfo = PdfDocument.PageInfo.Builder(
                bm.width,
                bm.height,
                i + 1 + 2
            ).create()
            val page = doc.startPage(pageInfo)
            page.canvas.drawBitmap(bm, 0.0f, 0.0f, Paint())
            doc.finishPage(page)
        }*/
        directory.resolve(PDF_FILENAME).outputStream().use { doc.writeTo(it) }
    }

    private fun generatePNGAttestation() {
        val bm = Bitmap.createBitmap(template.pageSize[0], template.pageSize[1], Bitmap.Config.ARGB_8888)
        bm.applyCanvas { drawForm(this) }
        val f = directory.resolve(ATTESTATION_PNG_FILENAME)
        f.outputStream().use { bm.compress(Bitmap.CompressFormat.PNG, 0, it) }

        val qr = generateQRCode(300 to 300)
        val f2 = directory.resolve(QRCODE_PNG_FILENAME)
        f2.outputStream().use { qr.compress(Bitmap.CompressFormat.PNG, 0, it) }
    }

    /** Generate the attestion for all the formats (and remove the old attestation) */
    fun generateAttestation() {
        generatePNGAttestation()
        generatePDFAttestation()
        removeOldAttestations(context)
    }

    fun loadAttestationBitmap(): Bitmap {
        directory.resolve(ATTESTATION_PNG_FILENAME).inputStream().use {
            return BitmapFactory.decodeStream(it)
        }
    }

    fun loadQRCodeBitmap(context: Context): Bitmap {
        directory.resolve(QRCODE_PNG_FILENAME).inputStream().use {
            return BitmapFactory.decodeStream(it)
        }
    }

    fun launchPDFIntent(context: Context, action: String) {
        val uri = FileProvider.getUriForFile(context,
            "fr.covidat.android.fileprovider", directory.resolve(PDF_FILENAME))

        val intent = Intent(action)
        intent.setDataAndType(uri, context.contentResolver.getType(uri))
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        if (action == Intent.ACTION_SEND) intent.putExtra(Intent.EXTRA_STREAM, uri)
        try {
            context.startActivity(intent)
        } catch (e: ActivityNotFoundException) {
            Toast.makeText(context, "Pas d'activité disponible pour cette action", Toast.LENGTH_LONG).show()
        }
    }

    fun copyPDFAttestation(uri: Uri) {
        val outputStream = context.contentResolver.openOutputStream(uri)
        val inputStream = directory.resolve(PDF_FILENAME).inputStream()
        inputStream?.use { val i = it; outputStream?.use { i.copyTo(it) } }
    }
}