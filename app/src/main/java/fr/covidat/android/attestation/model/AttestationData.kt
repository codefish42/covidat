/*
 * Copyright (c) 2020 Codefish <codefish@online.fr>
 * Software released under the terms of the CeCILL 2.1 license [https://cecill.info/]
 */

package fr.covidat.android.attestation.model

import android.content.Context
import android.preference.PreferenceManager
import com.google.gson.Gson
import fr.covidat.android.R
import fr.covidat.android.frenchtowns.model.Town
import fr.covidat.android.personaldata.model.PersonalData
import fr.covidat.android.personaldata.model.loadPersonalData
import fr.covidat.android.templates.*
import fr.covidat.android.utils.asDateRanges
import fr.covidat.android.utils.isContemporary
import java.io.Serializable
import java.lang.Exception
import java.util.*

enum class AttestationKind(
    periodSpec: String,
    val validReasons: Array<Reason>,
    val template: AttestationTemplate,
    val usualName: Int,
    val explanation: Int,
    val navId: Int,
    val durationAlert: (Reason) -> Int /* in milliseconds, -1 for no alert */,
    val distanceAlert: (Reason) -> Int /* in meters, -1 for no alert */) {

    original("2020/03/17-2020/05/11", arrayOf(
        Reason.travail,
        Reason.courses,
        Reason.sante,
        Reason.famille,
        Reason.sport,
        Reason.convocation,
        Reason.missions
    ),
        OriginalAttestationTemplate,
        R.string.original_attestation_name,
        R.string.original_attestation_explanation,
        R.id.nav_generator_original,
        { if (it == Reason.sport) 3600000 else -1 },
        { if (it == Reason.sport) 1000 else -1 }),

    longDistance("2020/05/11-2020/06/02", arrayOf(
        Reason.travail,
        Reason.travailRecurrent,
        Reason.ecole,
        Reason.sante,
        Reason.famille,
        Reason.police,
        Reason.convocation,
        Reason.missions
    ),
        LongDistanceAttestationTemplate,
        R.string.longdistance_attestation_name,
        R.string.longdistance_attestation_explanation,
        R.id.nav_generator_longdistance,
        { -1 },
        { -1 }),

    publicTransportParis("2020/05/11-2020/06/02", arrayOf(
        Reason.travail,
        Reason.ecole,
        Reason.sante,
        Reason.famille,
        Reason.police,
        Reason.convocation,
        Reason.missions
    ),
        PublicTransportParisAttestationTemplate,
        R.string.publictransportparis_attestation_name,
        R.string.publictransportparis_attestation_explanation,
        R.id.nav_generator_publictransportparis,
        { -1 },
        { -1}),

    curfew("2020/10/17-2020/10/30;2020/12/15-", arrayOf(
        Reason.travail,
        Reason.sante,
        Reason.famille,
        Reason.handicap,
        Reason.convocation,
        Reason.missions,
        Reason.transits,
        Reason.animaux
    ),
      CurfewAttestationTemplate,
        R.string.curfew_attestation_name,
        R.string.curfew_attestation_explanation,
        R.id.nav_generator_curfew,
        { if (it == Reason.animaux) 3600000 else -1 },
        { if (it == Reason.animaux) 1000 else -1}),

    lockdown2("2020/10/30-2020/12/15", arrayOf(
        Reason.travail,
        Reason.courses,
        Reason.sante,
        Reason.famille,
        Reason.handicap,
        Reason.sport,
        Reason.convocation,
        Reason.missions,
        Reason.enfants
    ),
        Lockdown2bAttestationTemplate,
        R.string.lockdown2_attestation_name,
        R.string.lockdown2_attestation_explanation,
        R.id.nav_generator_lockdown2,
        { if (it == Reason.sport) (3600000 * 3) else -1 },
        { if (it == Reason.sport) (1000 * 20) else -1 });

    val periods = periodSpec.asDateRanges

    /** Is this attestation useful for the current moment ? */
    val isContemporary: Boolean get() = periods.isContemporary
}

enum class Reason(val description: String, customReasonName: String? = null) {
    travail("Déplacement professionnel ou scolaire"), // original,longDistance,curfew,lockdown2
    travailRecurrent("Déplacement professionnel récurrent", "travail"), // longDistance
    courses("Achats, culture et cultes", "achats_culturel_cultuel"), // original, lockdown2
    sante("Urgence médicale"), // original, longDistance, curfew, lockdown2
    famille("Motif familial impérieux"), // original, longDistance, curfew, lockdown2
    sport("Activité physique et promenade d'animaux (<20 km, <3 h)", "sport_animaux"), // original, lockdown2
    convocation("Convocation judiciaire ou administrative"), // original, longDistance, curfew, lockdown2
    missions("Mission d'intérêt général"), // original, longDistance, curfew, lockdown2
    police("Convocation policière"), // longDistance
    ecole("Motif scolaire"),

    handicap("Déplacement de personnes handicapées"), // curfew, lockdown2
    transits("Déplacement de longue distance"), // curfew
    animaux("Accompagnement d'animal de compagnie"), // curfew

    enfants("Accompagnement d'enfants à l'école"); // lockdown2

    val reasonName: String = customReasonName ?: name
}


/** Circumstances for the attestation */
data class Circumstances(
    val reasons: EnumSet<Reason>,
    val date: Long, // timestamp in millis of the date
    val destination: Town?
): Serializable {

    private var _calendar: Calendar? = null

    private val calendar: Calendar get() {
        val c = _calendar
        if (c != null) return c
        else {
            val c = Calendar.getInstance()
            c.timeInMillis = date
            _calendar = c
            return c
        }
    }

    val hour get() = calendar[Calendar.HOUR_OF_DAY].toString().padStart(2, '0')
    val minute get() = calendar[Calendar.MINUTE].toString().padStart(2, '0')
    val hourMinute get() = "${hour}:$minute"
    val day get() = "${calendar[Calendar.DAY_OF_MONTH].toString().padStart(2, '0')}/${(calendar[Calendar.MONTH] + 1).toString().padStart(2, '0')}/${calendar[Calendar.YEAR]}"

    val reasonsText get() = reasons.joinToString("-") { it.reasonName }
    val duration get() = System.currentTimeMillis() - date
}

data class AttestationData(
    val personalData: PersonalData,
    val circumstances: Circumstances,
    val attestationKind: AttestationKind = AttestationKind.original
): Serializable
{
    companion object {
        fun create(context: Context, attestationKind: AttestationKind? = null, reasons: EnumSet<Reason>? = null, destination: Town? = null): AttestationData? {
            val personalData = loadPersonalData(context)
            if (personalData == null) return null
            val latestAttestationData = Attestation.getLatestAttestation(context)?.attestationData
            val r = reasons ?: // we load the reasons from the latest attestation
                latestAttestationData?.circumstances?.reasons
                ?: return null
            val kind = attestationKind ?: latestAttestationData?.attestationKind ?: return null
            val d = destination ?: latestAttestationData?.circumstances?.destination
            return AttestationData(
                personalData,
                Circumstances(
                    r,
                    System.currentTimeMillis(),
                    d
                ),
                kind
            )
        }
    }

}

private val gson = Gson()


fun loadReasons(context: Context): EnumSet<Reason> {
    val s = PreferenceManager.getDefaultSharedPreferences(context).getString("reasons", null)
    val reasons = EnumSet.noneOf(Reason::class.java)
    if (s != null)
        s.split("-").map { try { Reason.valueOf(it) } catch (e: Exception) {null} }.filterNotNull().forEach { reasons.add(it) }
    return reasons
}

fun EnumSet<Reason>.save(context: Context) {
    val editor = PreferenceManager.getDefaultSharedPreferences(context).edit()
    editor.putString("reasons", this.map { it.name }.joinToString("-"))
    editor.commit()
}

fun loadDestinationTown(context: Context): Town? {
    val s = PreferenceManager.getDefaultSharedPreferences(context).getString("destinationTown", null)
    return if (s != null) gson.fromJson(s, Town::class.java) else null
}

fun Town.save(context: Context) {
    val editor = PreferenceManager.getDefaultSharedPreferences(context).edit()
    editor.putString("destinationTown", gson.toJson(this))
    editor.commit()
}