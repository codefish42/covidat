/*
 * Copyright (c) 2020 Codefish <codefish@online.fr>
 * Software released under the terms of the CeCILL 2.1 license [https://cecill.info/]
 */

package fr.covidat.android.attestation.task

import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.util.Log
import android.widget.Toast
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import fr.covidat.android.attestation.model.Attestation
import fr.covidat.android.attestation.model.AttestationData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.Serializable
import java.lang.Exception

data class AttestationTaskResult(val generated: Boolean = false, val exception: Exception? = null) {
    companion object {
        /** Action used for broadcast */
        val NEW_ATTESTATION_ACTION = AttestationTaskResult::class.java.name + ".new_attestation"
    }

    override fun toString(): String {
        val state = mutableListOf<String>()
        if (generated) state.add("générée")
        if (exception != null) state.add("exception ${exception}: ${exception.message}")
        return "Attestation : ${state.joinToString(", ")}"
    }
}

private suspend fun AttestationData.generateAttestation(context: Context): AttestationTaskResult =
    withContext(Dispatchers.Default) {
        val res = try {
            val attestation = Attestation.create(context, this@generateAttestation)
            attestation.generateAttestation()
            val generated = true
            AttestationTaskResult(generated) // no exception
        } catch (e: Exception) {
            Log.e(javaClass.name, "Failure when generating the attestation", e)
            AttestationTaskResult(exception = e)
        }
        res
    }

suspend fun AttestationData.makeAttestation(context: Context): AttestationTaskResult =
    withContext(Dispatchers.Main) {
        val res = generateAttestation(context)
        if (res.exception == null) {
            val intent = Intent(AttestationTaskResult.NEW_ATTESTATION_ACTION)
            intent.putExtra("attestationData", this@makeAttestation as Serializable)
            LocalBroadcastManager.getInstance(context).sendBroadcast(intent)
        }
        Toast.makeText(context, res.toString(), Toast.LENGTH_LONG).show()
        res
    }
