/*
 * Copyright (c) 2020 Codefish <codefish@online.fr>
 * Software released under the terms of the CeCILL 2.1 license [https://cecill.info/]
 */

package fr.covidat.android.attestation.ui

import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import fr.covidat.android.*
import fr.covidat.android.attestation.task.AttestationTaskResult
import fr.covidat.android.attestation.model.*
import fr.covidat.android.attestation.task.makeAttestation
import fr.covidat.android.frenchtowns.model.Town
import fr.covidat.android.frenchtowns.ui.TownDialogFragment
import fr.covidat.android.personaldata.model.PersonalData
import fr.covidat.android.personaldata.model.loadPersonalData
import kotlinx.android.synthetic.main.fragment_attestation_generator.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.*

class AttestationGeneratorFragment : Fragment(), TownDialogFragment.OnTownSelectionListener {
    companion object {
        const val PERSONAL_DATA_REQUEST_ID = 1
    }

    var personalData: PersonalData? = null
    lateinit var attestationKind: AttestationKind
    var destination: Town? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val defaultAttestationKind = AttestationKind.values().find { it.isContemporary } ?: AttestationKind.longDistance
        attestationKind = AttestationKind.valueOf(
            arguments?.getString("attestationKind", defaultAttestationKind.name) ?: defaultAttestationKind.name)
    }


    private fun getCheckedReasons(): EnumSet<Reason> {
        val reasons = EnumSet.noneOf(Reason::class.java)
        Reason.values().forEach {
            if ((reasonsContainer.findViewWithTag<View>(it.name) as? Checkable)?.isChecked == true)
                reasons.add(it)
        }
        return reasons
    }

    private fun refreshView() {
        personalDataTextView.text = if (personalData?.isValid ?: false)
            personalData.toString()
        else
            "Données personnelles incomplètes\nCliquez sur modifier pour les indiquer"
        val validDest = attestationKind.template.destinationTown.isEmpty() || destination != null
        generateButton.isEnabled = (personalData?.isValid == true && getCheckedReasons().size > 0 && validDest)
        townEditText.setText(destination?.toString() ?: "")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_attestation_generator, container, false)
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val reasons = loadReasons(requireContext())
        val radios = RadioGroup(requireContext())
        radios.orientation = RadioGroup.VERTICAL
        attestationKind.validReasons.forEach {
            val box = RadioButton(context)
            box.tag = it.name
            box.text = it.description
            box.setOnClickListener { refreshView() }
            radios.addView(box)
        }
        reasonsContainer.addView(radios)
        reasons.forEach { radios.findViewWithTag<RadioButton>(it.name)?.isChecked = true }

        destination = loadDestinationTown(requireContext())

        personalDataModifyButton.setOnClickListener {
            val action =
                AttestationGeneratorFragmentDirections.gotoProfile()
            findNavController().navigate(action)
        }

        generateButton.setOnClickListener {
            val data = AttestationData.create(requireContext(), attestationKind, getCheckedReasons(), destination)
            CoroutineScope(Dispatchers.Main).launch {
                val res = data?.makeAttestation(requireContext())
                if (res?.generated == true) {
                    val action =
                        AttestationGeneratorFragmentDirections.gotoAttestation()
                    findNavController().navigate(action)
                }
            }
            getCheckedReasons().save(requireContext())
        }

        // display or not the town container
        townContainer.visibility = if (attestationKind.template.destinationTown.isNotEmpty()) View.VISIBLE else View.GONE

        townEditText.setOnClickListener {
            val dialog = TownDialogFragment()
            dialog.arguments = bundleOf("selection" to destination, "referenced" to true)
            dialog.setTargetFragment(this, 1)
            dialog.show(parentFragmentManager, "townPicker")
        }

        // show the explanation
        attestationExplanationTextView.setText(attestationKind.explanation)

        // display or not the not contemporary warning
        notContemporaryTextView.visibility = if (attestationKind.isContemporary) View.GONE else View.VISIBLE
    }

    override fun onTownSelected(town: Town?) {
        destination = town
        destination?.save(requireContext())
        refreshView()
    }

    fun refreshPersonalData() {
        personalData = loadPersonalData(requireContext())
        refreshView()
    }

    val prefListener = SharedPreferences.OnSharedPreferenceChangeListener { sharedPreferences, key ->
        if (key == "personalData") refreshPersonalData()
    }

    override fun onStart() {
        super.onStart()
        // prefill the personal data
        refreshPersonalData()
        PreferenceManager.getDefaultSharedPreferences(requireContext()).registerOnSharedPreferenceChangeListener(prefListener)
    }

    override fun onStop() {
        super.onStop()
        PreferenceManager.getDefaultSharedPreferences(requireContext()).unregisterOnSharedPreferenceChangeListener(prefListener)
    }

}