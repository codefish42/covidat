/*
 * Copyright (c) 2020 Codefish <codefish@online.fr>
 * Software released under the terms of the CeCILL 2.1 license [https://cecill.info/]
 */

package fr.covidat.android.frenchtowns.model



/** A French department with its information */
data class Department(val code: String, val name: String, val capital: String, val region: String) {
    override fun toString() = "$code: $name ($region)"
}



