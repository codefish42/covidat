/*
 * Copyright (c) 2020 Codefish <codefish@online.fr>
 * Software released under the terms of the CeCILL 2.1 license [https://cecill.info/]
 */

package fr.covidat.android.frenchtowns.model

import android.content.Context
import android.util.Log
import fr.covidat.android.utils.successor
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.FileNotFoundException

class DepartmentTowns(towns: Sequence<Town>) {
    val sortedTowns = towns.sorted().toList()

    fun getPrefixRange(prefix: String): IntRange {
        fun convert(x: Int) = if (x >= 0) x else -x-1
        val start = convert(sortedTowns.binarySearch { it.name.compareTo(prefix, ignoreCase = true) })
        val prefixSuccessor = prefix.successor
        val stop = convert(if (start == sortedTowns.size) sortedTowns.size else sortedTowns.binarySearch(start) { it.name.compareTo(prefixSuccessor, ignoreCase = true) })
        return start until stop
    }

    companion object {
        const val ASSET_FOLDER = "frenchTowns"

        private fun loadFromLine(l: String): Town? {
            if (l.isBlank()) return null
            val e = l.split(';')
            if (e.size < 2) return null
            val location = try {
                Coords(e[2].toFloat(), e[3].toFloat())
            } catch (e: Exception) {
                null
            }
            return Town(
                name = e[0],
                zip = e[1],
                location = location
            )
        }

        private suspend fun loadFromAsset(context: Context, department: String): DepartmentTowns =
            withContext(Dispatchers.Default) {
                try {
                    context.assets.open("$ASSET_FOLDER/${department}.towns.csv").bufferedReader()
                        .useLines {
                            DepartmentTowns(it.mapNotNull { l ->
                                loadFromLine(
                                    l
                                )
                            })
                        }
                } catch (e: FileNotFoundException) {
                    DepartmentTowns(emptySequence())
                }
            }

        fun asyncLoadFromAsset(context: Context, department: String, callback: (DepartmentTowns?) -> Unit) {
            CoroutineScope(Main).launch { callback(loadFromAsset(context, department)) }
        }
    }
}