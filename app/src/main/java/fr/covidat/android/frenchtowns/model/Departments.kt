/*
 * Copyright (c) 2020 Codefish <codefish@online.fr>
 * Software released under the terms of the CeCILL 2.1 license [https://cecill.info/]
 */

package fr.covidat.android.frenchtowns.model

import android.content.Context

class Departments(departments: Sequence<Department>) {
    val departmentMap = departments.associateBy { it.code }.toSortedMap()

    companion object {
        const val ASSET_PATH = "frenchDepartments/departments.csv"

        private var cachedInstance: Departments? = null

        fun getInstance(context: Context): Departments {
            val instance = cachedInstance
            if (instance != null) return instance
            val loadedInstance =
                loadFromAsset(context)
            cachedInstance = loadedInstance
            return loadedInstance
        }

        private fun loadFromLine(line: String): Department? {
            if (line.isBlank()) return null
            val e = line.split(',')
            if (e.size < 5) return null
            return Department(
                code = e[0],
                name = e[2],
                capital = e[3],
                region = e[4]
            )
        }

        private fun loadFromAsset(context: Context): Departments {
            return context.assets.open(ASSET_PATH).bufferedReader().useLines {
                Departments(it.mapIndexedNotNull { i, l ->
                    if (i > 0) loadFromLine(
                        l
                    ) else null
                })
            }
        }
    }
}