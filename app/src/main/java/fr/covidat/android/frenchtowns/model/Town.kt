/*
 * Copyright (c) 2020 Codefish <codefish@online.fr>
 * Software released under the terms of the CeCILL 2.1 license [https://cecill.info/]
 */

package fr.covidat.android.frenchtowns.model

import fr.covidat.android.utils.GeoUtils
import java.io.Serializable

data class Coords(val latitude: Float, val longitude: Float): Serializable

/** A French town */
data class Town(val name: String, val zip: String, val location: Coords?): Serializable, Comparable<Town> {

    override fun compareTo(other: Town): Int {
        val cmp = name.compareTo(other.name)
        if (cmp != 0) return cmp
        return zip.compareTo(other.zip)
    }

    override fun toString() = "$name ($zip)"

    val isLocated: Boolean get() = location != null
}

fun Coords.distanceTo(other: Coords) =
    GeoUtils.haversine(latitude.toDouble(), longitude.toDouble(), other.latitude.toDouble(), other.longitude.toDouble())

fun Town.distanceTo(other: Town) =
    if (location != null && other.location != null) location.distanceTo(other.location) else Double.NaN
