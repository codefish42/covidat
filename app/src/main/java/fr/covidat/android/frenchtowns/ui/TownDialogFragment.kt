/*
 * Copyright (c) 2020 Codefish <codefish@online.fr>
 * Software released under the terms of the CeCILL 2.1 license [https://cecill.info/]
 */

package fr.covidat.android.frenchtowns.ui

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import fr.covidat.android.frenchtowns.model.Town
import android.view.Gravity
import android.view.WindowManager
import android.R.attr.x
import android.graphics.Point
import android.view.Display
import fr.covidat.android.frenchtowns.model.Coords


/**
 * A fragment displaying a list of French towns organized by departments.
 */
class TownDialogFragment : DialogFragment() {

    interface OnTownSelectionListener { fun onTownSelected(town: Town?) }

    private val selection = MutableLiveData<Town?>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        arguments?.run {
            selection.value = (getSerializable("selection") as? Town)
            val v = TownViewHelper(
                context!!, inflater, container,
                referenced = getBoolean("referenced"),
                _referenceLocation = getFloatArray("referenceLocation")?.run { Coords(this[0], this[1]) },
                selection = selection
            ).view
            var discard = 1
            selection.observe(this@TownDialogFragment, Observer {
                (targetFragment as? OnTownSelectionListener)?.onTownSelected(it)
                if (discard-- == 0) dismiss()
            })
            v
        }

    override fun onResume() {
        val window = dialog!!.window
        val size = Point()
        val display = window!!.windowManager.defaultDisplay
        display.getSize(size)
        // Set the width of the dialog proportional to 75% of the screen width
        window.setLayout((size.x * 0.75).toInt(), WindowManager.LayoutParams.MATCH_PARENT)
        window.setGravity(Gravity.CENTER)
        super.onResume()
    }

}