/*
 * Copyright (c) 2020 Codefish <codefish@online.fr>
 * Software released under the terms of the CeCILL 2.1 license [https://cecill.info/]
 */

package fr.covidat.android.frenchtowns.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import fr.covidat.android.frenchtowns.model.Coords

/**
 * A fragment displaying a list of French towns organized by departments.
 */
class TownFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        arguments?.run {
            TownViewHelper(
                context!!, inflater, container,
                referenced = getBoolean("referenced"),
                _referenceLocation = getFloatArray("referenceLocation")?.run { Coords(this[0], this[1]) },
                selection = null
            ).view
        }
}
