/*
 * Copyright (c) 2020 Codefish <codefish@online.fr>
 * Software released under the terms of the CeCILL 2.1 license [https://cecill.info/]
 */

package fr.covidat.android.frenchtowns.ui

import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import fr.covidat.android.R
import fr.covidat.android.frenchtowns.model.*
import fr.covidat.android.personaldata.model.loadPersonalData
import kotlin.math.ceil

class TownViewHelper(
    val context: Context,
    inflater: LayoutInflater,
    container: ViewGroup?,
    val referenced: Boolean = false,
    _referenceLocation: Coords? = null,
    val selection: MutableLiveData<Town?>? = null) {

    val referenceLocation: Coords? = if (referenced) {
        _referenceLocation ?: loadPersonalData(context!!)?.town?.location
    } else
        null

    val view = inflater.inflate(R.layout.fragment_town, container, false).apply {
        val departments = Departments.getInstance(context!!).departmentMap.values.toList()

        val spinner = findViewById<Spinner>(R.id.departmentSpinner)
        val recyclerView = findViewById<RecyclerView>(R.id.townRecyclerView)
        val townPrefixTextView = findViewById<EditText>(R.id.townSearchEditText)

        spinner.adapter = ArrayAdapter(context!!, android.R.layout.simple_list_item_1, departments)

        spinner.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                recyclerView.adapter = null
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val dpt = departments[position]
                DepartmentTowns.asyncLoadFromAsset(context!!, dpt.code) {
                    val prefix = townPrefixTextView.text.toString()
                    recyclerView.layoutManager = LinearLayoutManager(context!!)
                    recyclerView.adapter = it?.run {
                        val a = TownAdapter(
                            it,
                            referenceLocation,
                            selection = selection
                        )
                        a.range = getPrefixRange(prefix)
                        a
                    }
                }
            }

        }

        // Change the list of towns according to the entered prefix
        townPrefixTextView.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                (recyclerView.adapter as? TownAdapter)?.apply {
                    range = towns.getPrefixRange(s.toString())
                }
            }

        })

        // Select the department of the selected town with the spinner
        selection?.value?.apply {
            spinner.setSelection(departments.indexOfFirst { this.zip.startsWith(it.code) })
        }
    }

    class TownAdapter(val towns: DepartmentTowns, val referenceLocation: Coords?, val selection: MutableLiveData<Town?>?): RecyclerView.Adapter<TownAdapter.TownViewHolder>() {

        var range: IntRange = towns.sortedTowns.indices
            set(value) {
                field = value
                notifyDataSetChanged()
            }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            TownViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_town, parent, false))

        override fun getItemCount() = range.count()

        override fun onBindViewHolder(holder: TownViewHolder, position: Int) {
            holder.update(position, towns.sortedTowns[range.first + position])
        }

        inner class TownViewHolder(val view: View): RecyclerView.ViewHolder(view) {
            val townTextView = view.findViewById<TextView>(R.id.townTextView)
            val distanceTextView = view.findViewById<TextView>(R.id.distanceTextView)

            fun update(position: Int, town: Town) {
                view.setOnClickListener {
                    selection?.apply {
                        value = town
                        notifyDataSetChanged()
                    }
                }
                townTextView.text = "${town.name} (${town.zip})"
                townTextView.typeface = if (selection?.value == town) Typeface.DEFAULT_BOLD else Typeface.DEFAULT
                if (referenceLocation != null) {
                    distanceTextView.visibility = View.VISIBLE
                    val d = ceil(town?.location?.distanceTo(referenceLocation)?.div(1000.0) ?: -1.0).toInt()
                    val color = if (town.isLocated) DISTANCE_COLORS.entries.first { d in it.key }.value else Color.TRANSPARENT
                    distanceTextView.text = "${(if (town.isLocated) d.toString() else "?").padStart(4,' ')} km"
                    distanceTextView.setBackgroundColor(color)
                } else
                    distanceTextView.visibility = View.GONE
            }
        }

    }

    /** Limit is set to 20 km from the home */
    companion object {
        val DISTANCE_COLORS = mapOf(
            (0 until 15) to Color.GREEN,
            (15 until 20) to Color.YELLOW,
            (20 until Integer.MAX_VALUE) to 0xFFFF7F00.toInt())
    }

}