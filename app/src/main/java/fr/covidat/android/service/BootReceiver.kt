/*
 * Copyright (c) 2020 Codefish <codefish@online.fr>
 * Software released under the terms of the CeCILL 2.1 license [https://cecill.info/]
 */

package fr.covidat.android.service

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

/** To start automatically the service on boot */
class BootReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        GeneratorService.start(context, true)
    }
}
