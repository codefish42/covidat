/*
 * Copyright (c) 2020 Codefish <codefish@online.fr>
 * Software released under the terms of the CeCILL 2.1 license [https://cecill.info/]
 */

package fr.covidat.android.templates

import fr.covidat.android.attestation.model.AttestationData
import fr.covidat.android.attestation.model.Reason

abstract class AttestationTemplate {
    abstract val resource: Int

    abstract val pageSize: IntArray
    open val offset = intArrayOf(0, 0)

    abstract val reason: Map<Reason, IntArray>

    open val firstName = intArrayOf()
    open val lastName = intArrayOf()
    open val completeName = intArrayOf()

    open val birthDate = intArrayOf()
    open val birthPlace = intArrayOf()
    open val completeBirth = intArrayOf()

    open val street = intArrayOf()
    open val town = intArrayOf()
    open val completeAddress = intArrayOf()

    abstract val place: IntArray

    open val date = intArrayOf()
    open val dayOfMonth = intArrayOf()
    open val month = intArrayOf()
    open val year = intArrayOf()
    open val hour = intArrayOf()
    open val minute = intArrayOf()
    open val hourMinute = intArrayOf()

    open val destinationTown = intArrayOf()
    open val destinationDepartment = intArrayOf()

    open val creationDateLabel = intArrayOf()
    open val creationDate = intArrayOf()

    abstract val qr: IntArray
    open val qr2 = intArrayOf(400, 400, 800, 800)

    abstract fun getQRText(data: AttestationData): String
}