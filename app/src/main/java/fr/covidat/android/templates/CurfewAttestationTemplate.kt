/*
 * Copyright (c) 2020 Codefish <codefish@online.fr>
 * Software released under the terms of the CeCILL 2.1 license [https://cecill.info/]
 */

package fr.covidat.android.templates

import fr.covidat.android.R
import fr.covidat.android.attestation.model.AttestationData
import fr.covidat.android.attestation.model.Reason

object CurfewAttestationTemplate: AttestationTemplate() {

    override val resource = R.raw.curfew_certificate

    override val pageSize = intArrayOf(1644, 2327)
    override val offset = intArrayOf(10, 28)

    private val reasonBasePos = intArrayOf(194, -1, 25, 25)

    fun r(a: Int): IntArray {
        val c = reasonBasePos.copyOf()
        c[1] = a
        return c
    }

    override val reason = mapOf(
        Reason.travail to r(811),
        Reason.sante to r(948),
        Reason.famille to r(1125),
        Reason.handicap to r(1263),
        Reason.convocation to r(1368),
        Reason.missions to r(1466),
        Reason.transits to r(1566),
        Reason.animaux to r(1664)
    )
    
    override val completeName = intArrayOf(330, 450, 1119, 38)
    override val birthDate = intArrayOf(327, 513, 464, 38)
    override val birthPlace = intArrayOf(862, 513, 582, 38)
    override val street = intArrayOf(363, 580, 1079, 38)
    override val town = intArrayOf(363, 616, 1079, 38)

    override val place = intArrayOf(289, 1810, 903, 38)
    override val date = intArrayOf(255, 1878, 443, 33)
    override val hourMinute = intArrayOf(861, 1878, 228, 33)

    override val qr = intArrayOf(1186, 1716, 300, 300)

    override fun getQRText(data: AttestationData): String =
        data.run {
            arrayOf(
                "Cree le: ${circumstances.day} a ${circumstances.hourMinute}",
                "Nom: ${personalData.lastName}",
                "Prenom: ${personalData.firstName}",
                "Naissance: ${personalData.birthDate} a ${personalData.birthPlace}",
                "Adresse: ${personalData.street}\n${personalData?.town?.zip} ${personalData?.town?.name}",
                "Sortie: ${circumstances.day} a ${circumstances.hourMinute}",
                "Motifs: ${circumstances.reasonsText}"
            ).joinToString(";\n ")
        }
}