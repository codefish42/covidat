/*
 * Copyright (c) 2020 Codefish <codefish@online.fr>
 * Software released under the terms of the CeCILL 2.1 license [https://cecill.info/]
 */

package fr.covidat.android.templates

import fr.covidat.android.R
import fr.covidat.android.attestation.model.AttestationData
import fr.covidat.android.attestation.model.Reason

object Lockdown2AttestationTemplate: AttestationTemplate() {

    override val resource = R.raw.lockdown2_certificate

    override val pageSize = intArrayOf(1636, 2315)
    override val offset = intArrayOf(10, 28)

    private val reasonBasePos = intArrayOf(218, -1, 40, 40)

    fun r(a: Int): IntArray {
        val c = reasonBasePos.copyOf()
        c[1] = a
        return c
    }

    override val reason = mapOf(
        Reason.travail to r(696),
        Reason.courses to r(819),
        Reason.sante to r(973),
        Reason.famille to r(1085),
        Reason.handicap to r(1198),
        Reason.sport to r(1304),
        Reason.convocation to r(1477),
        Reason.missions to r(1584),
        Reason.enfants to r(1708)
    )
    
    override val completeName = intArrayOf(324, 371, 1119, 42)
    override val birthDate = intArrayOf(324, 430, 431, 42)
    override val birthPlace = intArrayOf(815, 430, 597, 42)
    override val completeAddress = intArrayOf(361, 492, 1056, 42)

    override val place = intArrayOf(287, 1808, 867, 38)
    override val date = intArrayOf(247, 1868, 364, 38)
    override val hourMinute = intArrayOf(714, 1868, 364, 38)

    override val qr = intArrayOf(1181, 1700, 300, 300)

    override fun getQRText(data: AttestationData): String =
        data.run {
            arrayOf(
                "Cree le: ${circumstances.day} a ${circumstances.hourMinute}",
                "Nom: ${personalData.lastName}",
                "Prenom: ${personalData.firstName}",
                "Naissance: ${personalData.birthDate} a ${personalData.birthPlace}",
                "Adresse: ${personalData.street} ${personalData.town?.zip} ${personalData.town?.name}",
                "Sortie: ${circumstances.day} a ${circumstances.hourMinute}",
                "Motifs: ${circumstances.reasonsText}"
            ).joinToString(";\n")
        }
}