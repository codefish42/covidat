/*
 * Copyright (c) 2020 Codefish <codefish@online.fr>
 * Software released under the terms of the CeCILL 2.1 license [https://cecill.info/]
 */

package fr.covidat.android.templates

import fr.covidat.android.R
import fr.covidat.android.attestation.model.AttestationData
import fr.covidat.android.attestation.model.Reason

object Lockdown2bAttestationTemplate: AttestationTemplate() {

    override val resource = R.raw.lockdown2b_certificate

    override val pageSize = intArrayOf(1628, 2303)
    override val offset = intArrayOf(10, 28)

    private val reasonBasePos = intArrayOf(114, -1, 23, 23)

    fun r(a: Int): IntArray {
        val c = reasonBasePos.copyOf()
        c[1] = a
        return c
    }

    override val reason = mapOf(
        Reason.travail to r(767),
        Reason.courses to r(961),
        Reason.sante to r(1094),
        Reason.famille to r(1160),
        Reason.handicap to r(1260),
        Reason.sport to r(1326),
        Reason.convocation to r(1525),
        Reason.missions to r(1592),
        Reason.enfants to r(1658)
    )
    
    override val completeName = intArrayOf(250, 353, 1258, 39)
    override val birthDate = intArrayOf(248, 404, 276, 39)
    override val birthPlace = intArrayOf(581, 404, 865, 39)
    override val completeAddress = intArrayOf(283, 454, 1234, 39)

    override val place = intArrayOf(210, 2066, 986, 39)
    override val date = intArrayOf(169, 2116, 400, 39)
    override val hourMinute = intArrayOf(620, 2116, 400, 39)

    override val qr = intArrayOf(1176, 1954, 300, 300)

    override fun getQRText(data: AttestationData): String =
        data.run {
            arrayOf(
                "Cree le: ${circumstances.day} a ${circumstances.hourMinute}",
                "Nom: ${personalData.lastName}",
                "Prenom: ${personalData.firstName}",
                "Naissance: ${personalData.birthDate} a ${personalData.birthPlace}",
                "Adresse: ${personalData.street} ${personalData.town?.zip} ${personalData.town?.name}",
                "Sortie: ${circumstances.day} a ${circumstances.hourMinute}",
                "Motifs: ${circumstances.reasonsText}"
            ).joinToString(";\n")
        }
}