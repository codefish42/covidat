/*
 * Copyright (c) 2020 Codefish <codefish@online.fr>
 * Software released under the terms of the CeCILL 2.1 license [https://cecill.info/]
 */

package fr.covidat.android.templates

import fr.covidat.android.R
import fr.covidat.android.attestation.model.AttestationData
import fr.covidat.android.attestation.model.Reason

object LongDistanceAttestationTemplate: AttestationTemplate() {

    override val resource = R.raw.longdistance_certificate

    override val pageSize = intArrayOf(1645, 2326)
    override val offset = intArrayOf(10, 28)

    private val reasonBasePos = intArrayOf(116, -1, 25, 25)

    fun r(a: Int): IntArray {
        val c = reasonBasePos.copyOf()
        c[1] = a
        return c
    }

    override val reason = mapOf(
        Reason.travail to r(1026),
        Reason.travailRecurrent to r(1026) + intArrayOf(1399, 866, 25, 25),
        Reason.ecole to r(1117),
        Reason.sante to r(1248),
        Reason.famille to r(1339),
        Reason.police to r(1430),
        Reason.convocation to r(1561),
        Reason.missions to r(1652)
    )
    
    override val lastName = intArrayOf(220, 495, 1228, 38)
    override val firstName = intArrayOf(288, 563, 1151, 38)
    override val completeBirth = intArrayOf(471, 630, 965, 38)
    override val street = intArrayOf(547, 698, 894, 38)
    override val town = intArrayOf(135, 753, 1307, 38)

    override val place = intArrayOf(210, 1758, 544, 38)

    override val destinationTown = intArrayOf(430, 901, 585, 29)
    override val destinationDepartment = intArrayOf(1287, 901, 158, 29)

    override val dayOfMonth = intArrayOf(324, 857, 86, 30, 810, 1758, 86, 30)
    override val month = intArrayOf(429, 857, 86, 30, 921, 1758, 86, 30)

    override val creationDateLabel = intArrayOf(1212, 1938, 255, 21)
    override val creationDate = intArrayOf(1212, 1963, 255, 21)

    override val qr = intArrayOf(1190, 1650, 300, 300)

    override fun getQRText(data: AttestationData): String =
        data.run {
            arrayOf(
                "Cree le: ${circumstances.day} a ${circumstances.hourMinute}",
                "Nom: ${personalData.lastName}",
                "Prenom: ${personalData.firstName}",
                "Naissance: ${personalData.birthDate} a ${personalData.birthPlace}",
                "Adresse: ${personalData.street} ${personalData?.town?.zip} ${personalData?.town?.name}",
                "Sortie: ${circumstances.day.removeSuffix("/2020")} vers ${circumstances.destination?.name} ${circumstances.destination?.zip} (${circumstances.destination?.zip?.substring(0..1)})",
                "Motifs: ${circumstances.reasonsText}"
            ).joinToString(";\n ")
        }
}