/*
 * Copyright (c) 2020 Codefish <codefish@online.fr>
 * Software released under the terms of the CeCILL 2.1 license [https://cecill.info/]
 */

package fr.covidat.android.templates

import fr.covidat.android.R
import fr.covidat.android.attestation.model.AttestationData
import fr.covidat.android.attestation.model.Reason

object OriginalAttestationTemplate: AttestationTemplate() {

    override val resource = R.raw.original_certificate

    override val pageSize = intArrayOf(1652, 2338)
    override val offset = intArrayOf(10, 28)

    private val reasonBasePos = intArrayOf(205, -1, 42, 42)

    fun r(a: Int): IntArray {
        val c = reasonBasePos.copyOf()
        c[1] = a
        return c
    }

    override val reason = mapOf(
        Reason.travail to r(840),
        Reason.courses to r(979),
        Reason.sante to r(1093),
        Reason.famille to r(1193),
        Reason.sport to r(1349),
        Reason.convocation to r(1477),
        Reason.missions to r(1578)
    )
    
    override val completeName = intArrayOf(400, 407, 1044, 38)
    override val birthDate = intArrayOf(400, 472, 1044, 38)
    override val birthPlace = intArrayOf(400, 540, 1044, 38)
    override val completeAddress = intArrayOf(400, 608, 1044, 38)

    override val place = intArrayOf(301, 1683, 1161, 38)

    override val date = intArrayOf(257, 1753, 219, 32)
    override val hour = intArrayOf(514, 1753, 72, 32)
    override val minute = intArrayOf(615, 1753, 72, 32)

    override val creationDateLabel = intArrayOf(1220, 1920, 300, 21)
    override val creationDate = intArrayOf(1220, 1945, 300, 21)

    override val qr = intArrayOf(1164, 1625, 300, 300)

    override fun getQRText(data: AttestationData): String =
        data.run {
            arrayOf(
                "Cree le: ${circumstances.day} a ${circumstances.hourMinute}",
                "Nom: ${personalData.lastName}",
                "Prenom: ${personalData.firstName}",
                "Naissance: ${personalData.birthDate} a ${personalData.birthPlace}",
                "Adresse: ${personalData.street} ${personalData?.town?.zip} ${personalData?.town?.name}",
                "Sortie: ${circumstances.day} a ${circumstances.hourMinute}",
                "Motifs: ${circumstances.reasonsText}"
            ).joinToString("; ")
        }
}