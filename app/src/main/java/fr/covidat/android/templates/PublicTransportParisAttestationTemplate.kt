/*
 * Copyright (c) 2020 Codefish <codefish@online.fr>
 * Software released under the terms of the CeCILL 2.1 license [https://cecill.info/]
 */

package fr.covidat.android.templates

import fr.covidat.android.R
import fr.covidat.android.attestation.model.AttestationData
import fr.covidat.android.attestation.model.Reason

object PublicTransportParisAttestationTemplate: AttestationTemplate() {
    override val resource = R.raw.publictransportparis_certificate

    override val pageSize = intArrayOf(1645, 2326)

    override val completeName = intArrayOf(331, 467, 1275, 36)
    override val birthDate = intArrayOf(298, 505, 1275, 36)
    override val birthPlace = intArrayOf(204, 541, 1275, 36)
    override val street = intArrayOf(329, 580, 1275, 36)
    override val town = intArrayOf(329, 618, 1275, 36)
    override val place = intArrayOf(254, 1684, 905, 37)
    override val date = intArrayOf(205, 1762, 905, 37)

    private val reasonBasePos = intArrayOf(151, -6, 16, 16)

    fun r(a: Int): IntArray {
        val c = reasonBasePos.copyOf()
        c[1] += a
        return c
    }

    override val reason = mapOf(
        Reason.travail to r(780),
        Reason.ecole to r(894),
        Reason.sante to r(1047),
        Reason.famille to r(1162),
        Reason.police to r(1275),
        Reason.convocation to r(1428),
        Reason.missions to r(1542)
    )

    override val creationDateLabel = intArrayOf(1236, 1953, 133, 18)
    override val creationDate = intArrayOf(1236, 1972, 215, 18)

    override val qr = intArrayOf(1165, 1656, 300, 300)

    override fun getQRText(data: AttestationData): String =
        data.run {
            arrayOf(
                "Cree le: ${circumstances.day} a ${circumstances.hourMinute}",
                "Nom: ${personalData.lastName}",
                "Prenom: ${personalData.firstName}",
                "Naissance: ${personalData.birthDate} a ${personalData.birthPlace}",
                "Adresse: ${personalData.street} ${personalData?.town?.zip} ${personalData?.town?.name}",
                "Sortie: ${circumstances.day.removeSuffix("/2020")}",
                "Motifs: ${circumstances.reasonsText}"
            ).joinToString(";\n ")
        }
}