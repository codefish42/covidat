/*
 * Copyright (c) 2020 Codefish <codefish@online.fr>
 * Software released under the terms of the CeCILL 2.1 license [https://cecill.info/]
 */

package fr.covidat.android.utils

import android.util.Range
import java.text.SimpleDateFormat

private val dateFormat = SimpleDateFormat("yyyy/MM/dd")

val String.asDateRange: LongRange
    get() {
    val elements = this.split('-')
    val start = if (elements[0].isBlank()) 0L else dateFormat.parse(elements[0]).time
    val stop = if (elements[1].isBlank()) Long.MAX_VALUE else dateFormat.parse(elements[1]).time
    return start until stop
}

val String.asDateRanges: List<LongRange> get() =
    this.split(';').map { it.asDateRange }

val LongRange.isContemporary: Boolean get() = System.currentTimeMillis() in this

val List<LongRange>.isContemporary: Boolean get() = this.any { it.isContemporary }