/*
 * Copyright (c) 2020 Codefish <codefish@online.fr>
 * Software released under the terms of the CeCILL 2.1 license [https://cecill.info/]
 */

package fr.covidat.android.utils

import java.lang.Math.pow
import java.lang.Math.toRadians
import kotlin.math.*

object GeoUtils {
    const val EARTH_RADIUS = 6_372_800.0 // in meters

    // Copied from https://github.com/acmeism/RosettaCodeData/blob/master/Task/Haversine-formula/Kotlin/haversine-formula.kotlin
    // Author: https://github.com/ingydotnet
    fun haversine(lat1: Double, lon1: Double, lat2: Double, lon2: Double): Double {
        val λ1 = toRadians(lat1)
        val λ2 = toRadians(lat2)
        val Δλ = toRadians(lat2 - lat1)
        val Δφ = toRadians(lon2 - lon1)
        return 2 * EARTH_RADIUS * asin(sqrt(sin(Δλ / 2).pow(2.0) + sin(Δφ / 2).pow(2.0) * cos(λ1) * cos(λ2)))
    }
}