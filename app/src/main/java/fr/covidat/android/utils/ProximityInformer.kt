/*
 * Copyright (c) 2020 Codefish <codefish@online.fr>
 * Software released under the terms of the CeCILL 2.1 license [https://cecill.info/]
 */

package fr.covidat.android.utils

import android.content.Context
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.util.Log


class ProximityInformer(val context: Context, distanceListener: (Float) -> Unit): AutoCloseable {
    companion object {
        const val UPDATE_TIME_INTERVAL = 60 * 1000L // in millis
        const val UPDATE_DISTANCE_INTERVAL = 200.0f // in meters

        fun create(context: Context, distanceListener: (Float) -> Unit): ProximityInformer? {
            val pi = ProximityInformer(context, distanceListener)
            return if (pi.working) pi else null
        }
    }

    val manager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
    val working: Boolean // if the location permission is granted or not
    var closed = false

    var initialLocation: Location? = null
    var latestLocation: Location? = null

    val locationListener = object: LocationListener {
        override fun onLocationChanged(location: Location?) {
            Log.v(javaClass.name, "Having received location: $location")
            if (initialLocation == null) initialLocation = location
            latestLocation = location
            distanceListener(currentDistance!!)
        }

        override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) { }

        override fun onProviderEnabled(provider: String?) { }

        override fun onProviderDisabled(provider: String?) { }

    }

    init {
        working = try {
            manager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                UPDATE_TIME_INTERVAL,
                UPDATE_DISTANCE_INTERVAL, locationListener)
            true
        } catch (e: SecurityException) {
            false
        }
    }

    override fun close() {
        if (closed) return
        if (working)
            manager.removeUpdates(locationListener)
        closed = true
    }

    /** Return the current distance to the initial point in meters */
    val currentDistance: Float? get() = initialLocation?.distanceTo(latestLocation)
}