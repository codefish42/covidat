/*
 * Copyright (c) 2020 Codefish <codefish@online.fr>
 * Software released under the terms of the CeCILL 2.1 license [https://cecill.info/]
 */

package fr.covidat.android.utils

import android.content.Context
import android.net.wifi.WifiManager
import android.os.SystemClock

private val String.cleanSSID: String get() {
    return trim().replace("\"", "")
}

enum class WifiConnectionState { UNCONNECTED, CONNECTED, UNAUTHORIZED }
data class WifiConnection(val state: WifiConnectionState, val ssid: String? = null, val bssid: String? = null) {
    val timestamp = SystemClock.elapsedRealtime()
    val duration get() = SystemClock.elapsedRealtime() - timestamp // in millis
}

/** Get the SSID and BSSID of the connected access point (return null if no connection) */
val Context.wifiConnection: WifiConnection get() {
    val wifiManager = applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
    try {
        val info = wifiManager.connectionInfo
        if (info == null) return WifiConnection(WifiConnectionState.UNAUTHORIZED)
        if (info.bssid == "02:00:00:00:00:00") return WifiConnection(WifiConnectionState.UNAUTHORIZED)
        if (info.bssid == null) return WifiConnection(WifiConnectionState.UNCONNECTED)
        if (info.ssid == "<unknown ssid>") return WifiConnection(WifiConnectionState.UNCONNECTED)
        return WifiConnection(WifiConnectionState.CONNECTED, info.ssid.cleanSSID, info.bssid)
    } catch (e: SecurityException) {
        return WifiConnection(WifiConnectionState.UNAUTHORIZED)
    }
}