/*
 * Copyright (c) 2020 Codefish <codefish@online.fr>
 * Software released under the terms of the CeCILL 2.1 license [https://cecill.info/]
 */

package fr.covidat.android.viewer


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.github.chrisbanes.photoview.PhotoView
import fr.covidat.android.attestation.model.Attestation

/**
 * A simple fragment displaying the attestation
 */
class AttestationFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val v = Attestation.getLatestAttestation(requireContext())?.loadAttestationBitmap()?.let {
                val photoView = PhotoView(context)
                photoView.setImageBitmap(it)
                photoView.setZoomable(true)

                photoView
            }
        return if (v == null) {
            val tv = TextView(context)
            tv.text = "Pas encore d'attestation générée"
            tv
        } else v
    }


}
