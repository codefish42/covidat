Une application générant des attestations de déplacement dérogatoire dans le cadre de l'état d'urgence sanitaire en France lié à l'épidémie de Covid-19.
Les attestations générées suivent le modèle proposé par le ministère de l'intérieur français.
Les attestations peuvent être générées manuellement ou automatiquement par un service Android en premier-plan lors de la déconnexion du réseau wifi domestique.
L'application est sans publicité et n'envoie aucune donnée vers Internet (pas de permission INTERNET utilisée).

Fonctionnalités :

* Génération d'attestations avec données personnelles fournies :
    * Attestation de confinement complet (obsolète)
    * Attestation pour déplacement > 100 km (obsolète)
    * Attestation de déplacement en transports en commun en Île-de-France en heures de pointe (obsolète)
    * Attestation de déplacement durant le couvre-feu (de 21h à 6h)
* Visualisation de la dernière attestation et du QR code correspondant
* Export de l'attestation PDF dans un fichier ou envoi vers une application
* Notification d'attestation avec QR code accessible depuis l'écran de verrouillage
* Génération automatique d'attestation lors de la sortie du domicile (perte du réseau wifi domestique)
* Alertes sonores lors de la génération automatique et le dépassement de temps/distance

Attention : il ne s'agit pas d'une application officiellement supportée par l'administration française.
Malgré le plus grand soin apporté à la réalisation de cette application, aucune garantie n'est apportée par l'auteur ni par les distributeurs de l'application.

Le dépôt Git de l'application est accessible à [https://gitlab.com/codefish42/covidat]